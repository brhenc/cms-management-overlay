Overlay for CMS (Wordpress, Joomla, Drupal etc.) related ebuilds that ease 
backups /deployment/upgrades. Instead of relying on portage to provide a CMS 
directly, this overlay aims to use a CLI tool instead that allows upgrading of 
CMS installs. Therefore, upgrading a CMS install does not require root
privileges, which should allow proper shared hosting behavior with Funtoo Linux.
